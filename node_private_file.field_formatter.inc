<?php

/**
 * Field Theme Overrides
 * 'filefield_formatter_url_plain', 'filefield_formatter_path_plain', 'filefield_item', 'filefield_file'
 */

/**
 * Theme function for the 'path_plain' formatter.
 */
function theme_node_private_file_filefield_formatter_path_plain($element) {
  // Inside a View this function may be called with null data. In that case,
  // just return.
  if (empty($element['#item'])) {
    return '';
  }

  $item = $element['#item'];
  $field_name = $element['#field_name'];
  // if we don't have a nid fallback to the default filefield theme
  if (empty($item['nid']) || empty($element['#node'])) {
    return theme_filefield_formatter_path_plain($element);
  }

  if (!node_private_file_field_private($field_name, $element['#node']->type)) {
    return theme_filefield_formatter_path_plain($element);
  }

  $field = content_fields($element['#field_name']);
  $item = $element['#item'];
  // If there is no image on the database, use default.
  if (empty($item['fid']) && $field['use_default_file']) {
    $item = $field['default_file'];
  }
  if (empty($item['filepath']) && !empty($item['fid'])) {
    $item = array_merge($item, field_file_load($item['fid']));
  }

  return empty($item['filepath']) ? '' : check_plain(node_private_file_private_path($item['nid'], $item['filepath'], $field_name));
}

/**
 * Theme function for the 'url_plain' formatter.
 */
function theme_node_private_file_filefield_formatter_url_plain($element) {
  // Inside a View this function may be called with null data. In that case,
  // just return.
  if (empty($element['#item'])) {
    return '';
  }

  $item = $element['#item'];
  $field_name = $element['#field_name'];
  // if we don't have a nid fallback to the default filefield theme
  if (empty($item['nid']) || empty($element['#node'])) {
    return theme_filefield_formatter_url_plain($element);
  }

  if (!node_private_file_field_private($field_name, $element['#node']->type)) {
    return theme_filefield_formatter_url_plain($element);
  }

  $field = content_fields($field_name);
  // If there is no image on the database, use default.
  if (empty($item['fid']) && $field['use_default_file']) {
    $item = $field['default_file'];
  }
  if (empty($item['filepath']) && !empty($item['fid'])) {
    $item = array_merge($item, field_file_load($item['fid']));
  }

  if (empty($item['filepath'])) {
    return '';
  }

  $private_filepath = node_private_file_private_path($item['nid'], $item['filepath'], $field_name);

  //@TODO: see whether we can use file_create_url (currently no)
  return url($private_filepath, array('absolute' => TRUE));
}

/**
 * Overrides theme_filefield_file
 * This theme overrides the theme for filefield file.
 */
function theme_node_private_file_filefield_file($file) {
  if (empty($file['fid'])) {
    return '';
  }

  if (empty($file['nid'])) {
    return theme_filefield_file($file);
  }

  // don't reset the cache here because our node might be already loaded
  $node = node_load($file['nid']);
  // clear static node_load cache
  node_load(NULL, NULL, TRUE);

  $filefield_fields = _node_private_file_node_type_filefields($node->type);
  // for all filefields in this content type
  foreach ($filefield_fields as $field_name => $field) {
    // it is cheaper to check which field is private first
    if (!node_private_file_field_private($field_name, $node->type)) {
      continue;
    }

    // we already know the item delta so make sure we skip if it is not in the current field
    if (empty($node->{$field_name}[$file['#delta']])) {
      continue;
    }

    // found a possible item
    $item = $node->{$field_name}[$file['#delta']];
    // compare fids
    if ($item['fid'] != $file['fid']) {
      continue;
    }

    // found our private item
    $file['filepath'] = node_private_file_private_path($node->nid, $item['filepath'], $field_name);
    return theme('node_private_file_protected_file', $file);
  }

  // fallback to the default filefield theme
  return theme_filefield_file($file);
}

/**
 * Theme a protected file link. This function is a copy of theme_filefield_file with the exception that
 * we provide our own url prefix.
 */
function theme_node_private_file_protected_file($file) {
  // Views may call this function with a NULL value, return an empty string.
  if (empty($file['fid'])) {
    return '';
  }

  $path = $file['filepath'];
  $url = url($path, array('absolute' => TRUE));
  $icon = theme('filefield_icon', $file);

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  // TODO: Possibly move to until I move to the more complex format described
  // at http://darrelopry.com/story/microformats-and-media-rfc-if-you-js-or-css
  $options = array(
    'attributes' => array(
      'type' => $file['filemime'] . '; length=' . $file['filesize'],
    ),
  );

  // Use the description as the link text if available.
  if (empty($file['data']['description'])) {
    $link_text = $file['filename'];
  }
  else {
    $link_text = $file['data']['description'];
    $options['attributes']['title'] = $file['filename'];
  }

  return '<div class="filefield-file">' . $icon . l($link_text, $url, $options) . '</div>';
}
